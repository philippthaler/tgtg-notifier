from datetime import date, datetime, timedelta
from pushover import Client
import re

from bs4 import BeautifulSoup

import config
from mail_fetcher import get_email_by_sender

sender = config.get('sender')

reg_time = r'\d+:\d+ AM|\d+:\d+ PM'
reg_date = r'\d+/\d+'
reg_address = r'You can pick up your order at (.+) any time between'


def parse_time(text):
    date = re.search(reg_date, text)
    times = re.findall(reg_time, text)
    date_str_from = str("{0} {1} {2}".format(date[0], str(datetime.now().year), times[0]))
    date_str_to = str("{0} {1} {2}".format(date[0], str(datetime.now().year), times[1]))
    date_from = datetime.strptime(date_str_from, '%m/%d %Y %I:%M %p')
    date_to = datetime.strptime(date_str_to, '%m/%d %Y %I:%M %p')
    return {
        'date_from': date_from,
        'date_to': date_to
    }


def parse_address(text):
    return re.findall(reg_address, text)[0]


def send_push_notification(name, address, date_from, date_to):
    client = Client(user_key=config.get('pushover_user_key'), api_token=config.get('pushover_token'))
    client.send_message(
        'Your order from {0} at {1} is ready to be picked up from {2} until {3}!!!'.format(name, address,
                                                                                           date_from.strftime('%H:%M'),
                                                                                           date_to.strftime('%H:%M')),
        title='Too Good To Go!!!',
        priority=1
    )


emails = get_email_by_sender(sender)
for msg in emails:
    if date.fromtimestamp(msg.date.timestamp()) >= date.today() - timedelta(days=2):
        soup = BeautifulSoup(msg.html, 'html.parser')
        name = ''
        address = ''
        date_from = datetime.now() - timedelta(days=3)
        date_to = datetime.now() - timedelta(days=3)
        for span in soup.find_all('span'):
            text = str(span.text)

            if text.startswith('Your order for'):
                name = re.sub(r'Your order for | has been placed', '', text)
            if text.startswith('You can pick up your order at'):
                times = parse_time(text)
                date_from = times['date_from']
                date_to = times['date_to']
                address = parse_address(text)

            if date_from + timedelta(hours=1) > datetime.now():
                send_push_notification(name, address, date_from, date_to)

