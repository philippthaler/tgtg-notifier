import yaml

with open('config.yml', 'r') as ymlfile:
    cfg = yaml.safe_load(ymlfile)


def get(name):
    return cfg[name]
