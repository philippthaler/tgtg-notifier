from imap_tools import MailBox, AND
import config

email = config.get('email')
password = config.get('password')
host = config.get('host')
subject = config.get('subject')

mb = MailBox(host).login(email, password)


def get_email_by_sender(sender):
    return mb.fetch(
        criteria=AND(from_=sender, subject=subject),
        mark_seen=False,
        bulk=True
    )
